package util

import java.util.Properties

/**
  * Created by chenjianwen on 2016/3/2.
  *
  * 全局参数
  */
 object GlobalArgs {

 val properties = new Properties()
 properties.load(ClassLoader.getSystemResourceAsStream("conf.properties"))

  val master_url = "local[*]"
  val act_path = "E:\\intellij idea workspace\\搜索服务-毕业设计\\CollaborationFilter\\tt\\160214\\click_2016-02-14.txt"
  //val order_path = "hdfs://ycf-114:9000/data/recsys/odm/order/*/"
  val order_path ="test.txt"
 //val act_path = "hdfs://192.168.147.133:9000/tt/160214/click_2016-02-14.txt"
 //val order_path ="hdfs://192.168.147.133:9000/order"

  val THREE_MONTH = 3*30*24*3600
  val SIX_MONTH = 6*30*24*3600
  val NINE_MONTH = 9*30*24*3600
  val BASE_SCORE = 5

  val TYPE_CLICK = 0
  val TYPE_STORE1 = 1
  val TYPE_STORE2 = 2
  val TYPE_SHARE = 3
  val TYPE_BOOK = 4

  val TABLE_NAME = "collaborative_res"
  val FAMILY_NAME = "products"

  val HABSE_HOST="192.168.147.133:2181"
  val HBASE_HDFS_DIR = "hdfs://192.168.147.133:9000/hbase"




  val REDIS_IP = properties.getProperty("redis.home")
  val REDIS_PORT = properties.getProperty("redis.port")





}
