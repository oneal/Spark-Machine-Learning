package model

import akka.actor.{Props, ActorRef, Actor}
import akka.actor.Actor.Receive

/**
  * Created by chenjianwen on 2016/3/15.
  *
  *
  */
class InserRedisActor extends Actor{
  override def receive = {
    case mas:String =>{println(mas)}
  }
}

object InserRedisActor{

  private[this]  var actorHelper:ActorRef = _

  def getActorRef:ActorRef={
      if(actorHelper != null) actorHelper
      actorHelper = ActorSystemHelper.getActorSystem.actorOf(Props[InserRedisActor])
      actorHelper
  }
}
